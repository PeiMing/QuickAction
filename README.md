<p align="center">
    <img width="150" src="./app/src/main/res/mipmap-xxhdpi/ic_launcher_round.webp">
</p>

<h1 align="center">QuickAction</h1>

轻仿EMUI 8样式，基于DynamicShortcuts，支持快速启动用户安装的app。

## 截图
 ![image](images/preview01.jpg)&nbsp;&nbsp;&nbsp;![image](images/preview02.jpg)

## 更新日志

2019/9/18
 * 修复应用列表item点击无效和分隔线显示错乱的问题。

2019/9/17
 * 修改加载框样式；
 * 增加打包签名配置；
 * 更新gradle版本、适配api29。

2018/10/23
 * 增加沉浸式样式；
 * 更新app图标和增加round图标；
 * 修改应用列表item和加载框等样式；
 * 修复加载app列表时可能会引起的内存泄漏。

2018/5/23
 * 基本完成功能。