package com.codeape.quickaction.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.io.File;

public class Tool {

    /**
     * 使用SharedPreferences保存信息
     *
     * @param context Context
     * @param Key     键
     * @param Value   值
     */
    public static void saveSharedPreferences(Context context, String Key, String Value) {

        SharedPreferences mySharedPreferences = context.getSharedPreferences("config", 0);
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        editor.putString(Key, Value);
        editor.apply();
    }

    /**
     * 使用SharedPreferences读取信息
     *
     * @param context Context
     * @param Key     键
     * @return name 键值
     */
    public static String readSharedPreferences(Context context, String Key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("config", 0);
        String name = sharedPreferences.getString(Key, "");
        if (name.equals("")) {
            return "null";
        } else {
            return name;
        }
    }


    /**
     * 判断shared_prefs是否存在
     */
    public static Boolean folderIsExist() {
        return new File("/data/data/" + "com.codeape.quickaction" + "/config").exists();
    }

    /**
     * 置空配置文件所有value
     *
     * @param context Context
     */
    public static void SetConfigNull(Context context) {
        for (int i = 0; i < 4; i++) {
            saveSharedPreferences(context, String.valueOf(i), "null");
        }
    }

    /**
     * 设置沉浸颜色
     *
     * @param window Content
     * @param color1 状态栏颜色
     * @param color2 导航栏颜色
     */
    public static void setImmersionStyle(Window window, int color1, int color2) {
        boolean isLightColor = nearWhite(Color.red(color1)) && nearWhite(Color.green(color1)) && nearWhite(Color.blue(color1));

        if ((window.getAttributes().flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) > 0) {
            return;
        }

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);// 取消设置透明状态栏,使 ContentView 内容不再覆盖状态栏
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);// 需要设置这个 flag 才能调用 setStatusBarColor 来设置状态栏颜色
        window.setStatusBarColor(color1);// 设置状态栏颜色
        window.setNavigationBarColor(color2);// 设置导航栏颜色

        // 设置浅色状态栏时的界面显示
        View decor = window.getDecorView();
        int ui = decor.getSystemUiVisibility();
        if (isLightColor) {
            ui |= 8192;
        } else {
            ui &= ~8192;
        }
        decor.setSystemUiVisibility(ui);

    }

    private static boolean nearWhite(int singleColor) {
        return singleColor > 200;
    }

}