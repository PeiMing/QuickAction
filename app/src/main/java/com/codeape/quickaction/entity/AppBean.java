package com.codeape.quickaction.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by 424571227@qq.com on 2018/4/17.
 */

public class AppBean {
    private Drawable appIcon;
    private String appName;
    private int appSize;
    private boolean isSd = false;
    private boolean isSystem = false;
    private String appPackageName;

    public String getApkPath() {
        return apkPath;
    }

    public void setApkPath(String apkPath) {
        this.apkPath = apkPath;
    }

    private String apkPath;

    public String getAppPackageName() {
        return appPackageName;
    }

    public void setAppPackageName(String appPackageName) {
        this.appPackageName = appPackageName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getAppSize() {
        return appSize;
    }

    public void setAppSize(int appSize) {
        this.appSize = appSize;
    }

    public boolean isSd() {
        return isSd;
    }

    public void setSd() {
        isSd = true;
    }

    public boolean isSystem() {
        return isSystem;
    }

    public void setSystem() {
        isSystem = true;
    }
}