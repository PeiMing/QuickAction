package com.codeape.quickaction;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Objects;

/**
 * Created by 424571227@qq.com on 2018/4/17.
 */
public class DynamicShortcutLike extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String AppPackageName = String.valueOf(Objects.requireNonNull(getIntent().getExtras()).getSerializable("AppPackageName"));

        Intent intent = getPackageManager().getLaunchIntentForPackage(AppPackageName);
        if (intent == null) {
            Toast.makeText(getApplicationContext(), "应用未安装", Toast.LENGTH_SHORT).show();
        }
        startActivity(intent);
        finish();
    }
}
