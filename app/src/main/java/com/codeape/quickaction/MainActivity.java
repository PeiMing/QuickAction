
package com.codeape.quickaction;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.NinePatchDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codeape.quickaction.entity.AppBean;
import com.codeape.quickaction.utils.Tool;
import com.codeape.quickaction.view.MProgressWheel;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    TextView tvTitle;
    RelativeLayout rlLoad;
    ListView listView;
    Button butSave;
    static List<AppBean> mAppBeanList = new ArrayList<>();
    List<ShortcutInfo> mShortcutInfos = new ArrayList<>();

    static boolean[] checks;//定义数组来保存CheckBox的状态
    static int number = 0;
    List<Integer> mTargetIndex = new ArrayList<>();
    boolean isExist;
    static List<String> mPackageNames = new ArrayList<>();
    static int i2 = 1;
    MProgressWheel loadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tool.setImmersionStyle(this.getWindow(), getResources().getColor(R.color.color_white), getResources().getColor(R.color.color_white));

        tvTitle = findViewById(R.id.tv_title);
        rlLoad = findViewById(R.id.rl_load);
        loadDialog = findViewById(R.id.progress_wheel);
        listView = findViewById(R.id.list);
        isExist = Tool.folderIsExist();
        for (int i = 0; i < 4; i++) {
            mPackageNames.add(Tool.readSharedPreferences(MainActivity.this, String.valueOf(i)));
        }

//        loadDialog = new Dialog(this, R.style.LoadDialog);// 初始化和设置style
//        loadDialog.setContentView(R.layout.view_loaddialog);// 设置layout
//        loadDialog.setCanceledOnTouchOutside(false);// 点击对话框外不消失

        new operationTask(MainActivity.this).execute();

        butSave = findViewById(R.id.but_save);

        butSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (number == 0) {
                    Toast.makeText(MainActivity.this, "请至少选择一个APP，以便使用该应用的桌面快捷功能", Toast.LENGTH_SHORT).show();
                } else if (number > 4) {
                    Toast.makeText(MainActivity.this, "最多只能选择4个", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < checks.length; i++) {
                        if (checks[i]) {
                            mTargetIndex.add(i);
                        }
                    }
                    Tool.SetConfigNull(MainActivity.this);//先重置配置文件避免数据错误
                    initDynamicShortcuts();
                }
            }
        });

    }

    void initDynamicShortcuts() {

        ShortcutManager scManager = this.getSystemService(ShortcutManager.class);
        for (int i = 0; i < mTargetIndex.size(); i++) {
            Drawable d = (mAppBeanList.get(mTargetIndex.get(i)).getAppIcon());
//            BitmapDrawable bd = null;
//            new ColorDrawable(android.graphics.Color.TRANSPARENT);
//            try {
//            bd = (BitmapDrawable) d;
//            bd.getBitmap();
//            } catch (ClassCastException ignored) {
//            }

            ShortcutInfo scInfoOne = null;
//            try {
            scInfoOne = (new ShortcutInfo.Builder(this, "dynamic" + i))
                    .setShortLabel(mAppBeanList.get(mTargetIndex.get(i)).getAppName())
                    .setLongLabel(mAppBeanList.get(mTargetIndex.get(i)).getAppName())
//                    .setIcon(Icon.createWithBitmap(bd.getBitmap()))
                    .setIcon(Icon.createWithBitmap(drawable2Bitmap(d)))
                    .setIntent((new Intent("android.intent.action.VIEW"))
                            .setClass(this, DynamicShortcutLike.class)
                            .putExtra("AppPackageName", mAppBeanList.get(mTargetIndex.get(i)).getAppPackageName()))
                    .build();
//            } catch (NullPointerException ignored) {
//
//            }
//            assert scManager != null;
            mShortcutInfos.add(scInfoOne);
            Tool.saveSharedPreferences(MainActivity.this, String.valueOf(i), mAppBeanList.get(mTargetIndex.get(i)).getAppPackageName());
        }
//        assert scManager != null;
        assert scManager != null;
        scManager.setDynamicShortcuts(mShortcutInfos);

        Toast.makeText(MainActivity.this, "设置成功！", Toast.LENGTH_SHORT).show();
        finish();
    }

    public static List<AppBean> getAllApk(Context context) {
        List<AppBean> appBeanList = new ArrayList<>();
        AppBean bean;
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> list = packageManager.getInstalledPackages(0);
        for (PackageInfo p : list) {
            bean = new AppBean();
            bean.setAppIcon(p.applicationInfo.loadIcon(packageManager));
            bean.setAppName(packageManager.getApplicationLabel(p.applicationInfo).toString());
            bean.setAppPackageName(p.applicationInfo.packageName);
            bean.setApkPath(p.applicationInfo.sourceDir);
            File file = new File(p.applicationInfo.sourceDir);
            bean.setAppSize((int) file.length());
            int flags = p.applicationInfo.flags;
            //判断是否是属于系统的apk
            if ((flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                bean.setSystem();
            } else {
                bean.setSd();
                appBeanList.add(bean);
            }


        }
        return appBeanList;
    }

    Bitmap drawableToBitamp(Drawable drawable) {
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        return Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    }

    /**
     * Drawable 转 bitmap
     *
     * @param drawable
     * @return
     */
    public static Bitmap drawable2Bitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof NinePatchDrawable) {
            Bitmap bitmap = Bitmap
                    .createBitmap(
                            drawable.getIntrinsicWidth(),
                            drawable.getIntrinsicHeight(),
                            drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                                    : Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
                    drawable.getIntrinsicHeight());
            drawable.draw(canvas);
            return bitmap;
        } else {
            return null;
        }
    }

    private static class operationTask extends AsyncTask<String, Object, Long> {

        private final WeakReference<MainActivity> mActivty;

        private operationTask(MainActivity mActivty) {
            this.mActivty = new WeakReference<>(mActivty);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            loadDialog.show();
        }

        @Override
        protected Long doInBackground(String... strings) {
            mAppBeanList = getAllApk(mActivty.get());
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
//            loadDialog.dismiss();
            MainActivity activity = mActivty.get();
            activity.loadDialog.stopSpinning();
            activity.rlLoad.setVisibility(View.GONE);
            activity.tvTitle.setText(activity.tvTitle.getText() + "(" + String.valueOf(mAppBeanList.size()) + ")");
            activity.listView.setAdapter(new AppsAdapter(activity, mAppBeanList));
        }
    }

    static class AppsAdapter extends BaseAdapter {

        private Context mContext;
        List<AppBean> appBeans;

        AppsAdapter(Context context, List<AppBean> appBeans) {
            mContext = context;
            this.appBeans = appBeans;
            checks = new boolean[appBeans.size()];
        }

        @Override
        public int getCount() {
            if (null != appBeans) {
                return appBeans.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(R.layout.item_apps, null);
                holder.rlBase = convertView.findViewById(R.id.item_rl_base);
                holder.appellation_image = convertView.findViewById(R.id.item_appellation_iv_bg);
                holder.appellation_title = convertView.findViewById(R.id.item_appellation_tv_title);
                holder.apps_checkBox = convertView.findViewById(R.id.item_apps_checkbox);
                holder.view_divider = convertView.findViewById(R.id.v_divider);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.appellation_image.setImageDrawable(appBeans.get(position).getAppIcon());
            holder.appellation_title.setText(appBeans.get(position).getAppName());

            if (mPackageNames.size() != 0) {
                for (int i = 0; i < mPackageNames.size(); i++) {
                    if ((appBeans.get(position).getAppPackageName()).equals(mPackageNames.get(i))) {
//                    holder.apps_checkBox.setChecked(true);
                        checks[position] = true;
//                    number += 1;
                    }
                }
            }

            //首次加载完所有item后清空本地选中的app数据list
            if ((position == appBeans.size() - 1) & i2 == 1) {
//                for (int i = 0; i < mPackageNames.size(); i++) {
//                    mPackageNames.remove(i);
                mPackageNames.clear();
                i2 -= 1;
//                }
            }

            holder.rlBase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean b = !(checks[position]);
                    if (b) {
                        number += 1;
                    } else {
                        number -= 1;
                    }
                    checks[position] = b;
                    holder.apps_checkBox.setChecked(checks[position]);
                }
            });

//            holder.apps_checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
////                    checks[position] = b;
//                    boolean b2 = !(checks[position]);
//                    if (b2) {
//                        number += 1;
//                    } else {
//                        number -= 1;
//                    }
//                    checks[position] = b2;
////                    holder.apps_checkBox.setChecked(checks[position]);
//                }
//            });

            holder.apps_checkBox.setChecked(checks[position]);

            //隐藏/显示最后一个分隔线
            holder.view_divider.setVisibility((position == appBeans.size() - 1) ? View.GONE : View.VISIBLE);

            return convertView;
        }

        class ViewHolder {
            RelativeLayout rlBase;
            ImageView appellation_image;
            TextView appellation_title;
            CheckBox apps_checkBox;
            View view_divider;
        }

    }

}
